#include <gtest/gtest.h>
#include <iostream>
#include "Nombre.cpp"

int main( int argc, char * argv[] )
{
    Nombre n = 1536;
    std::cout << "n : " << n << "\n";
    unsigned int j = 2;
    Nombre a = n + j;
    // std::cout << "test2 : " << a << "\n";
    std::cout << "n : " << n << "\n";

    // ::testing::InitGoogleTest( &argc, argv );
    // return RUN_ALL_TESTS();
}

// TEST (TestNombre, TestConstructeurParCopie){
//     Nombre * nombre1 = new Nombre(39286493);
//     Nombre * nombre2 = new Nombre(*nombre1);
//     std::ostringstream os1; std::ostringstream os2;
//     os1 << nombre1;
//     os2 << nombre2;
//     EXPECT_EQ(os1.str(), "39286493");
//     EXPECT_EQ(os2.str(), os1.str());
// }

// TEST( TestNombre, TestNombreVide )
// {
//     Nombre n;
//     std::ostringstream os;
//     os << n;
//     EXPECT_EQ( os.str(), "0" );
// }
 
// TEST( TestNombre, TestNombreNul )
// {
//     Nombre n{ 0 };
//     std::ostringstream os;
//     os << n;
//     EXPECT_EQ( os.str(), "0" );
// }
 
// TEST( TestNombre, TestNombre12345678 )
// {
//     Nombre n{ 12345678 };
//     std::ostringstream os;
//     os << n;
//     EXPECT_EQ( os.str(), "12345678" );
// }

// TEST( TestNombre, TestLectureGrandNombre )
// {
//     std::string big{ "123456789123456789123456789123456789" };
//     std::istringstream in{ big };
//     Nombre n;
//     in >> n;
//     std::ostringstream os;
//     os << n;
//     EXPECT_EQ( os.str(), big );
// }