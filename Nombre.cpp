//function definition
//Class definition : separate header from source file
#include <iostream>
#include <ctype.h>

class Nombre {

public:
    Nombre(){//test by default = 0
        Nombre(0);
    }

    Nombre(unsigned long nombre){
        premier_ = new Chiffre(nombre);
    }

    Nombre(const Nombre &nombre){
        premier_ = new Chiffre(*nombre.premier_);
    }

    ~Nombre(){
        delete premier_;
    }

    //affectation
    void swap(Nombre &nombre){
        std::swap(premier_, nombre.premier_);
    }
 
    friend std::ostream & operator <<( std::ostream & out, const Nombre & n ){
        if (n.premier_) n.premier_ -> affiche(out);
        return out;
    }

    friend std::istream &operator>>(std::istream  &in, Nombre &nombre){
        while( in.good() ) {
        int c{ in.get() };
        if( std::isdigit( c )) {
            unsigned int d{ static_cast<unsigned int>( c - '0' )};
            // d contient le chiffre entre 0 et 9 qui vient d'être lu
            nombre.premier_ = new Chiffre(d, nombre.premier_);
        }
        else break;
        }
        return in;
    }

    // int getPremier(){
    //     return premier_->chiffre_;
    // }

    Nombre & operator = (const Nombre &n){
        Nombre tmp(n);
        swap(tmp);
        return *this;
    }

    // Partie 1.4 Calculs
    friend Nombre operator+ (Nombre &n, unsigned int i){
        int addition = n.premier_->chiffre_ + i;
        // std::cout << "addition : " <<addition << "\n";

        if (addition <= 9){
            n.premier_->chiffre_ = addition;
        } else {
            n.premier_->chiffre_ = addition % 10;
            n.premier_->suivant_->chiffre_ += addition/10; //rappeler récursivement l'opération
        }
        return n;
    }
    

    friend Nombre operator+= (Nombre &n, unsigned int i){
        Nombre ne = 0;
        return ne;
    }

    friend Nombre operator* (Nombre &n, unsigned int i){
        Nombre ne = 0;
        return ne;
    }

    friend Nombre operator*= (Nombre &n, unsigned int i){
        Nombre ne = 0;
        return ne;
    }

private:
    struct Chiffre {

        Chiffre(unsigned long nombre){
            // itération : récupérer chiffre par chiffre
            // recursivité : le chiffre à ajouter est le reste de la division par 10
            chiffre_ = nombre % 10;
            // std::cout << chiffre_;
            if (nombre < 10){
                suivant_ = nullptr ;
            }
            else suivant_ = new Chiffre(nombre / 10); //123 --> 12
        }

        Chiffre(const Chiffre &chiffre){
            chiffre_ = chiffre.chiffre_;
            if(chiffre.suivant_ != nullptr){
                suivant_ = new Chiffre(*chiffre.suivant_);
            }
            else suivant_ = nullptr;
        }

        //constructeur pour la lecture
        Chiffre(unsigned int c, Chiffre *premier){
            chiffre_ = c;
            suivant_ = premier;
        }
        
        std::ostream &affiche(std::ostream & out) const{
            if(suivant_) suivant_ -> affiche(out);
            return out << chiffre_;
        }
        

        unsigned int chiffre_;     // entre 0 et 9
        Chiffre * suivant_;        // chiffre de poids supérieur ou nullptr

    };
    Chiffre * premier_;

};

